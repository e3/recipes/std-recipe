# std conda recipe

Home: https://github.com/epics-modules/std

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS std module
